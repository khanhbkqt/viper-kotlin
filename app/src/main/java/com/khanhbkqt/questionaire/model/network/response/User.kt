package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class User(@SerializedName("id") val id: Long,
                @SerializedName("fullname") val fullName: String,
                @SerializedName("image_url") val imageUrl: String,
                @SerializedName("occupation") val occupation: String,
                @SerializedName("reputation") val reputation: Reputation)

data class Reputation(@SerializedName("score") val score: Int,
                      @SerializedName("bronze") val bronze: Int,
                      @SerializedName("silver") val silver: Int,
                      @SerializedName("gold") val gold: Int)