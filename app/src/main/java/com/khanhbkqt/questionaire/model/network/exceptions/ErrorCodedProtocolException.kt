package com.khanhbkqt.questionaire.model.network.exceptions

import com.khanhbkqt.questionaire.model.network.interceptors.ErrorModel

class ErrorCodedProtocolException(val errorCode: Int, message: String, val errorModel: ErrorModel) : ProtocolException(message)
