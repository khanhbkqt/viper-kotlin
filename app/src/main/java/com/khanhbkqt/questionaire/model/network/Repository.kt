package com.khanhbkqt.questionaire.model.network

import com.khanhbkqt.questionaire.model.network.response.QuestionFeedResponse
import rx.Observable
import javax.inject.Inject

interface Repository {
    fun getQuestionsFeed(): Observable<QuestionFeedResponse>
}

class DataRepository
@Inject constructor(private val api: API) : Repository {
    override fun getQuestionsFeed(): Observable<QuestionFeedResponse> {
        return api.getQuestionsFeed()
    }

}