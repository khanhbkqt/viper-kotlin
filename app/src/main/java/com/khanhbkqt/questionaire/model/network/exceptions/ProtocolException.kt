package com.khanhbkqt.questionaire.model.network.exceptions

import java.io.IOException

open class ProtocolException(message: String) : IOException(message)
