package com.khanhbkqt.questionaire.model.network

import com.khanhbkqt.questionaire.model.network.response.QuestionFeedResponse
import retrofit2.http.GET
import rx.Observable

interface API {

    @GET(PATH_QUESTIONS)
    fun getQuestionsFeed(): Observable<QuestionFeedResponse>

    companion object {
        private const val PATH_QUESTIONS = "questions/feed"
    }
}