package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class FollowedQuestionResponse(@SerializedName("unread_answers") val unreadAnswers: Int,
                                    @SerializedName("other_questions") val otherQuestions: Int,
                                    @SerializedName("questions") val questions: List<Question>)