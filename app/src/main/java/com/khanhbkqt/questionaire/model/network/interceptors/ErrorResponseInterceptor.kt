package com.khanhbkqt.questionaire.model.network.interceptors

import com.google.gson.Gson
import com.khanhbkqt.questionaire.model.network.exceptions.ErrorCodedProtocolException
import com.khanhbkqt.questionaire.model.network.exceptions.ProtocolException

import java.io.IOException
import java.net.HttpURLConnection

import javax.inject.Inject

import okhttp3.Interceptor
import okhttp3.Response

class ErrorResponseInterceptor @Inject internal constructor(private val gson: Gson) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.isSuccessful
                || response.code() == HttpURLConnection.HTTP_NOT_FOUND
                || response.code() == HttpURLConnection.HTTP_UNAUTHORIZED
                || response.isRedirect) {
            return response
        } else {
            val string = response.body().string()
            val errorModel = gson.fromJson(string, ErrorModel::class.java)

            if (errorModel == null) {
                // in some rare cases we can have response for failed request, e.g. no internet and no cached response
                if (!response.isSuccessful) {
                    throw IOException(string)
                }

                throw ProtocolException("Something wrong happen")
            } else {
                throw ErrorCodedProtocolException(response.code(), "Something wrong happen", errorModel)
            }
        }
    }
}
