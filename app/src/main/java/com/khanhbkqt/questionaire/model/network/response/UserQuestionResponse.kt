package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class UserQuestionResponse(@SerializedName("unread_answers_other_questions") val unreadAnswersOtherQuestions: Int,
                                @SerializedName("other_questions") val otherQuestions: Int,
                                @SerializedName("questions") val questions: List<Question>)