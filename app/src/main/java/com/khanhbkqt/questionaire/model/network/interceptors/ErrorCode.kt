package com.khanhbkqt.questionaire.model.network.interceptors

import android.content.Context
import android.support.annotation.StringRes

enum class ErrorCode(private val value: String, @param:StringRes private val resId: Int) {

    ;

    fun getLocalizedError(context: Context): String {
        return context.getString(resId)
    }

    companion object {

        fun fromString(value: String): ErrorCode? {
            for (code in ErrorCode.values()) {
                if (code.value.equals(value, ignoreCase = true)) {
                    return code
                }
            }
            return null
        }
    }
}
