package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class Category(@SerializedName("id") val id: Long,
                    @SerializedName("name") val name: String,
                    @SerializedName("image_url") val imageUrl: String)