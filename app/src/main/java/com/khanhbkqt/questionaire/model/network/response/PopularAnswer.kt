package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName
import java.util.*

data class PopularAnswer(@SerializedName("id") val id: Long,
                         @SerializedName("text") val text: String,
                         @SerializedName("created_on") val createdOn: Date,
                         @SerializedName("upvotes") val upVotes: Int,
                         @SerializedName("user") val user: User,
                         @SerializedName("question") val question: PopularQuestion,
                         @SerializedName("views") val views: Int)