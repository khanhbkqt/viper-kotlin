package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName
import java.util.*

data class Question(@SerializedName("id") val id: Long,
                    @SerializedName("title") val title: String,
                    @SerializedName("created_on") val createdOn: Date,
                    @SerializedName("category") val category: Category,
                    @SerializedName("answers") val answers: Int,
                    @SerializedName("unread_answers") val unreadAnswers: Int)