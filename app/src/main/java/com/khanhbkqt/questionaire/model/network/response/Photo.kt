package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class Photo(@SerializedName("caption") val caption: String,
                 @SerializedName("url") val url: String)