package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName

data class ActivityResponse(@SerializedName("activities") val activities: List<Activity>,
                            @SerializedName("other_activities") val otherActivities: Int)

data class Activity(@SerializedName("id") val id: Long,
                    @SerializedName("type") val type: ActivityType?,
                    @SerializedName("description") val description: String,
                    @SerializedName("votes") val votes: Int,
                    @SerializedName("badge_type") val badgeType: BadgeType?)

enum class ActivityType constructor(private val value: String) {
    @SerializedName("vote") VOTE("vote"),
    @SerializedName("badge") BADGE("badge");
}

enum class BadgeType constructor(private val value: String) {
    @SerializedName("bronze") BRONZE("bronze"),
    @SerializedName("gold") GOLD("gold"),
    @SerializedName("silver") SILVER("silver");
}