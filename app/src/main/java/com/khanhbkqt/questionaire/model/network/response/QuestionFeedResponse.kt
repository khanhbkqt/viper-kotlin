package com.khanhbkqt.questionaire.model.network.response

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

data class QuestionFeedResponse(@SerializedName("categories") val categories: List<Category>,
                                @SerializedName("notifications") val notifications: List<JSONObject>,
                                @SerializedName("user_questions") val userQuestions: UserQuestionResponse,
                                @SerializedName("followed_questions") val followedQuestion: FollowedQuestionResponse,
                                @SerializedName("activity") val activity: ActivityResponse,
                                @SerializedName("popular_questions") val popularQuestions: List<PopularQuestion>,
                                @SerializedName("popular_answers") val popularAnswers: List<PopularAnswer>,
                                @SerializedName("questions") val questions: List<PopularQuestion>)