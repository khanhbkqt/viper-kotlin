package com.khanhbkqt.questionaire.model.network.interceptors

import android.content.Context

import com.google.gson.annotations.SerializedName

class ErrorModel internal constructor(@field:SerializedName("error_message") var errorMessage: String?) {

    val errorCode: ErrorCode?
        get() = ErrorCode.fromString(errorMessage!!)

    fun getLocalizedMessage(context: Context): String? {
        val code = errorCode
        return code?.getLocalizedError(context) ?: errorMessage
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as ErrorModel?

        return if (errorMessage != null)
            errorMessage == that!!.errorMessage
        else
            that!!.errorMessage == null
    }

    override fun hashCode(): Int {
        return if (errorMessage != null) errorMessage!!.hashCode() else 0
    }
}
