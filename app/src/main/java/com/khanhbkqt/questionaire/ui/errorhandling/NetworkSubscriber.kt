package com.khanhbkqt.questionaire.ui.errorhandling

import com.khanhbkqt.questionaire.model.network.exceptions.ErrorCodedProtocolException
import com.khanhbkqt.questionaire.model.network.exceptions.ProtocolException
import rx.Subscriber
import java.util.logging.Level
import java.util.logging.Logger

class NetworkSubscriber<T>(private val subscriber: Subscriber<T>, private val networkView: NetworkView) : Subscriber<T>() {

    override fun onCompleted() {
        subscriber.onCompleted()
    }

    override fun onNext(t: T) {
        subscriber.onNext(t)
    }

    override fun onError(e: Throwable) {
        val errorMessage: String

        if (e is ErrorCodedProtocolException) {
            val errorModel = e.errorModel
            val errorCode = errorModel.errorCode

            errorMessage = if (errorCode != null) networkView.getErrorByErrorCode(errorCode) else errorModel.errorMessage ?: ""
        } else if (e is ProtocolException) {
            errorMessage = networkView.serverErrorMessage
        } else {
            errorMessage = networkView.networkErrorMessage
        }
        LOGGER.log(Level.WARNING, "network error", e)
        networkView.showIoError(errorMessage)
        subscriber.onError(e)
    }

    companion object {
        private val LOGGER = Logger.getLogger(NetworkSubscriber::class.java.name)
    }
}
