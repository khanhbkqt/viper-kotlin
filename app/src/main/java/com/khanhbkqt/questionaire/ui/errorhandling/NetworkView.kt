package com.khanhbkqt.questionaire.ui.errorhandling

import com.dzaitsev.rxviper.ViewCallbacks
import com.khanhbkqt.questionaire.model.network.interceptors.ErrorCode

interface NetworkView : ViewCallbacks {

    val networkErrorMessage: String

    val serverErrorMessage: String
    fun showIoError(errorMessage: String)

    fun getErrorByErrorCode(errorCode: ErrorCode): String
}

