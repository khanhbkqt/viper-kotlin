package com.khanhbkqt.questionaire.ui.errorhandling

import com.dzaitsev.rxviper.Router
import com.dzaitsev.rxviper.ViperPresenter

import rx.Subscriber
import rx.functions.Action0
import rx.functions.Action1

abstract class NetworkPresenter<V : NetworkView, R : Router> protected constructor()// Do nothing here
    : ViperPresenter<V, R>() {

    protected fun <T> makeNetworkSubscriber(subscriber: Subscriber<T>, view: V): NetworkSubscriber<T> {
        checkNotNull(view, "networkView")
        return NetworkSubscriber(subscriber, view)
    }

    protected fun <T> makeNetworkSubscriber(onNext: Action1<T>, view: V): NetworkSubscriber<T> {
        checkNotNull(onNext, "onNext")

        return makeNetworkSubscriber(object : Subscriber<T>() {
            override fun onCompleted() {
                // Nothing to do
            }

            override fun onError(e: Throwable) {
                // Nothing to do
            }

            override fun onNext(result: T) {
                onNext.call(result)
            }
        }, view)
    }

    protected fun <T> makeNetworkSubscriber(onNext: Action1<T>, onError: Action1<Throwable>,
                                            view: V): NetworkSubscriber<T> {
        checkNotNull(onNext, "onNext")
        checkNotNull(onError, "onError")

        return makeNetworkSubscriber(object : Subscriber<T>() {
            override fun onCompleted() {
                // Nothing to do
            }

            override fun onError(e: Throwable) {
                onError.call(e)
            }

            override fun onNext(result: T) {
                onNext.call(result)
            }
        }, view)
    }

    protected fun <T> makeNetworkSubscriber(onNext: Action1<T>, onError: Action1<Throwable>,
                                            onComplete: Action0, view: V): NetworkSubscriber<T> {
        checkNotNull(onNext, "onNext")
        checkNotNull(onError, "onError")
        checkNotNull(onComplete, "onComplete")

        return makeNetworkSubscriber(object : Subscriber<T>() {
            override fun onCompleted() {
                onComplete.call()
            }

            override fun onError(e: Throwable) {
                onError.call(e)
            }

            override fun onNext(result: T) {
                onNext.call(result)
            }
        }, view)
    }

    private//It's used!
    fun <T> checkNotNull(arg: T?, name: String) {
        if (arg == null) {
            throw IllegalArgumentException("$name can not be null")
        }
    }

    fun obtainStatistics() {

    }

}
