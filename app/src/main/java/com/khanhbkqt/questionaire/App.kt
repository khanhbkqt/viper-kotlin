package com.khanhbkqt.questionaire

import android.app.Application
import android.content.Context
import com.khanhbkqt.questionaire.di.component.AppComponent
import com.khanhbkqt.questionaire.di.component.DaggerAppComponent
import com.khanhbkqt.questionaire.di.module.AppModule
import com.khanhbkqt.questionaire.di.module.NetworkModule
import java.net.URL

class App : Application() {
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        buildAppComponent()
    }

    private fun buildAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(getBaseUrl()))
                .build()
        appComponent.inject(this)
    }

    private fun getBaseUrl(): URL = URL(getString(R.string.base_url))

    companion object {

        fun get(context: Context): App {
            return context.applicationContext as App
        }

        fun componentFromContext(context: Context): AppComponent {
            return get(context).appComponent
        }
    }
}