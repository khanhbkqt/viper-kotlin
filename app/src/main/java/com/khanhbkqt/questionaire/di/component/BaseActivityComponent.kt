package com.khanhbkqt.questionaire.di.component

import com.khanhbkqt.questionaire.di.module.BaseActivityModule
import com.khanhbkqt.questionaire.di.scope.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [(BaseActivityModule::class)])
interface BaseActivityComponent {
}