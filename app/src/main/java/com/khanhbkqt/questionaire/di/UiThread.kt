package com.khanhbkqt.questionaire.di

import javax.inject.Qualifier

@Qualifier
annotation class UiThread
