package com.khanhbkqt.questionaire.di.module

import android.support.v7.app.AppCompatActivity
import com.khanhbkqt.questionaire.di.scope.ActivityScope
import com.tbruyelle.rxpermissions.RxPermissions
import dagger.Module
import dagger.Provides

@ActivityScope
@Module
class BaseActivityModule(val activity: AppCompatActivity) {

    @Provides
    fun provideRxPermissions(): RxPermissions {
        return RxPermissions(activity)
    }

}