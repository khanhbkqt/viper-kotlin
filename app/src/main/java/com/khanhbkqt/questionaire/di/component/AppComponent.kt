package com.khanhbkqt.questionaire.di.component

import com.khanhbkqt.questionaire.App
import com.khanhbkqt.questionaire.di.module.AppModule
import com.khanhbkqt.questionaire.di.module.BaseActivityModule
import com.khanhbkqt.questionaire.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (AppModule::class),
            (NetworkModule::class)
        ])
interface AppComponent {
    fun plus(activityModule: BaseActivityModule): BaseActivityComponent
    fun inject(app: App)
}