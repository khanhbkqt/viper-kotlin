package com.khanhbkqt.questionaire.di.module

import android.content.Context

import com.khanhbkqt.questionaire.di.IoThread
import com.khanhbkqt.questionaire.di.UiThread

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

@Module
class AppModule(private val app: Context) {

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return app.applicationContext
    }

    @Provides
    @UiThread
    internal fun provideUiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @IoThread
    internal fun provideIoThread(): Scheduler {
        return Schedulers.io()
    }

}