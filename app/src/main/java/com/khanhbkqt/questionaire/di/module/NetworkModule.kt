package com.khanhbkqt.questionaire.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.khanhbkqt.questionaire.BuildConfig
import com.khanhbkqt.questionaire.model.network.API
import com.khanhbkqt.questionaire.model.network.DataRepository
import com.khanhbkqt.questionaire.model.network.Repository
import com.khanhbkqt.questionaire.model.network.interceptors.ErrorResponseInterceptor
import com.khanhbkqt.questionaire.utils.DateFormatUtils
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable.from
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = arrayOf(AppModule::class))
class NetworkModule(private val baseUrl: URL) {

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder()
                .setDateFormat(DateFormatUtils.SERVER_DATE_FORMAT)
                .create()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor,
                                     errorResponseInterceptor: ErrorResponseInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.addInterceptor(httpLoggingInterceptor)
        builder.addInterceptor(errorResponseInterceptor)
        builder.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        builder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson,
                                 okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl.toString())
                .client(okHttpClient)
                .build()
    }

    @Provides
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    @Provides
    internal fun provideAPI(retrofit: Retrofit) : API {
        return retrofit.create(API::class.java)
    }

    @Provides
    internal fun provideRepository(repository: DataRepository): Repository {
        return repository
    }

    companion object {
        private const val READ_TIMEOUT: Long = 60
        private const val CONNECTION_TIMEOUT: Long = 60
    }

}