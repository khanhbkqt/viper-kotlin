package com.khanhbkqt.questionaire.base

import android.os.Bundle
import android.support.v4.app.DialogFragment
import com.khanhbkqt.questionaire.App
import com.khanhbkqt.questionaire.di.component.AppComponent

abstract class ComponentFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupComponent(App.componentFromContext(context))
    }

    protected abstract fun setupComponent(appComponent: AppComponent)
}
