package com.khanhbkqt.questionaire.base

import com.khanhbkqt.questionaire.R
import com.khanhbkqt.questionaire.model.network.interceptors.ErrorCode
import com.khanhbkqt.questionaire.ui.errorhandling.NetworkView

abstract class NetworkActivity : BaseActivity(), NetworkView {

    override val networkErrorMessage: String
        get() = getResources().getString(R.string.no_internet_message_main_screen)

    override val serverErrorMessage: String
        get() = getResources().getString(R.string.error_not_2xx_or_3xx)

    override fun showIoError(errorMessage: String) {
    }

    override fun getErrorByErrorCode(errorCode: ErrorCode): String {
        return errorCode.getLocalizedError(this)
    }
}
