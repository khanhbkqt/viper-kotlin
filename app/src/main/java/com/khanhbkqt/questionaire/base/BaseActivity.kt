package com.khanhbkqt.questionaire.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.khanhbkqt.questionaire.App
import com.khanhbkqt.questionaire.di.component.BaseActivityComponent
import com.khanhbkqt.questionaire.di.module.BaseActivityModule

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var activityModule: BaseActivityModule

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Dagger setup
        activityModule = BaseActivityModule(this)
        setupComponent(App.componentFromContext(this).plus(activityModule))
    }

    //region ActionBar
    protected var displayHomeAsUpEnabled: Boolean = false
        set(value) {
            field = value
            supportActionBar?.setDisplayHomeAsUpEnabled(value)
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (displayHomeAsUpEnabled && item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    //endregion

    protected abstract fun setupComponent(component: BaseActivityComponent)
}