package com.khanhbkqt.questionaire.base

import com.khanhbkqt.questionaire.R
import com.khanhbkqt.questionaire.model.network.interceptors.ErrorCode
import com.khanhbkqt.questionaire.ui.errorhandling.NetworkView

abstract class NetworkComponentFragment : ComponentFragment(), NetworkView {

    override val networkErrorMessage: String
        get() = if (context != null)
            resources.getString(R.string.no_internet_message_main_screen)
        else
            ""

    override val serverErrorMessage: String
        get() = if (context != null) resources.getString(R.string.error_not_2xx_or_3xx) else ""

    override fun showIoError(errorMessage: String) {

    }

    override fun getErrorByErrorCode(errorCode: ErrorCode): String {
        return if (context != null) errorCode.getLocalizedError(context) else ""
    }
}
