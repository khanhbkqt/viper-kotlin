package com.khanhbkqt.questionaire.base

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.khanhbkqt.questionaire.R
import com.khanhbkqt.questionaire.model.network.interceptors.ErrorCode
import com.khanhbkqt.questionaire.ui.errorhandling.NetworkView

abstract class NetworkRelativeLayout : RelativeLayout, NetworkView {

    override val networkErrorMessage: String
        get() = resources.getString(R.string.no_internet_message_main_screen)

    override val serverErrorMessage: String
        get() = resources.getString(R.string.error_not_2xx_or_3xx)

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun showIoError(errorMessage: String) {
    }

    override fun getErrorByErrorCode(errorCode: ErrorCode): String {
        return errorCode.getLocalizedError(context)
    }
}
